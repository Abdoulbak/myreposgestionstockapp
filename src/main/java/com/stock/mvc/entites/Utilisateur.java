package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_utilisateur")
public class Utilisateur implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idUtilisateur;
	private String c_prenom;
	private String c_nom;
	private String c_email;
	private String c_mdp;
	private String c_photo;
	
	public Long getC_idUtilisateur() {
		return c_idUtilisateur;
	}

	public void setC_idUtilisateur(Long c_idUtilisateur) {
		this.c_idUtilisateur = c_idUtilisateur;
	}

	public String getC_prenom() {
		return c_prenom;
	}

	public void setC_prenom(String c_prenom) {
		this.c_prenom = c_prenom;
	}

	public String getC_nom() {
		return c_nom;
	}

	public void setC_nom(String c_nom) {
		this.c_nom = c_nom;
	}

	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public String getC_mdp() {
		return c_mdp;
	}

	public void setC_mdp(String c_mdp) {
		this.c_mdp = c_mdp;
	}

	public String getC_photo() {
		return c_photo;
	}

	public void setC_photo(String c_photo) {
		this.c_photo = c_photo;
	}
	
	
	
}
