package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_mouvementStock")
public class MouvementStock implements Serializable {

	private static final int ENTREE=1;
	private static final int SORTIE=2;
	
	@Id
	@GeneratedValue
	private Long c_idMouvementStock;

	@Temporal(TemporalType.TIMESTAMP)
    private Date c_date;
	
	private Double c_quantite;
	private int c_typeMvtStock;
	@ManyToOne
	@JoinColumn(name="c_idArticle")
	private Article c_article;
	 
	public Long getC_idMouvementStock() {
		return c_idMouvementStock;
	}

	public void setC_idMouvementStock(Long c_idMouvementStock) {
		this.c_idMouvementStock = c_idMouvementStock;
	}

	public Date getC_date() {
		return c_date;
	}

	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}

	public Double getC_quantite() {
		return c_quantite;
	}

	public void setC_quantite(Double c_quantite) {
		this.c_quantite = c_quantite;
	}

	public int getC_typeMvtStock() {
		return c_typeMvtStock;
	}

	public void setC_typeMvtStock(int c_typeMvtStock) {
		this.c_typeMvtStock = c_typeMvtStock;
	}

	public Article getC_article() {
		return c_article;
	}

	public void setC_article(Article c_article) {
		this.c_article = c_article;
	}
	
	
}
