package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_client")
public class Client implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idClient;
	private String c_prenom;
	private String c_nom;
	private String c_adresse;
	private String c_photo;
	private String c_email;
	
	@OneToMany(mappedBy="c_client")
	private List<CommandeClient> c_commandesClient;
	
	public Long getC_idClient() {
		return c_idClient;
	}

	public void setC_idClient(Long c_idClient) {
		this.c_idClient = c_idClient;
	}

	public String getC_prenom() {
		return c_prenom;
	}

	public void setC_prenom(String c_prenom) {
		this.c_prenom = c_prenom;
	}

	public String getC_nom() {
		return c_nom;
	}

	public void setC_nom(String c_nom) {
		this.c_nom = c_nom;
	}

	public String getC_adresse() {
		return c_adresse;
	}

	public void setC_adresse(String c_adresse) {
		this.c_adresse = c_adresse;
	}

	public String getC_photo() {
		return c_photo;
	}

	public void setC_photo(String c_photo) {
		this.c_photo = c_photo;
	}

	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public List<CommandeClient> getC_commandesClient() {
		return c_commandesClient;
	}

	public void setC_commandesClient(List<CommandeClient> c_commandesClient) {
		this.c_commandesClient = c_commandesClient;
	}
	
	
	
}
