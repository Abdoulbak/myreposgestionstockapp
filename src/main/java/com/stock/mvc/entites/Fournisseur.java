package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_fournisseur")
public class Fournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idFournisseur;
	private String c_prenom;
	private String c_nom;
	private String c_adresse;
	private String c_photo;
	private String c_email;
	
	@OneToMany(mappedBy="c_fournisseur")
	private List<CommandeFournisseur> c_commandesFournisseur;
	
	public Fournisseur() {
		super();
	}

	public Long getC_idFournisseur() {
		return c_idFournisseur;
	}

	public void setC_idFournisseur(Long c_idFournisseur) {
		this.c_idFournisseur = c_idFournisseur;
	}

	public String getC_prenom() {
		return c_prenom;
	}

	public void setC_prenom(String c_prenom) {
		this.c_prenom = c_prenom;
	}

	public String getC_nom() {
		return c_nom;
	}

	public void setC_nom(String c_nom) {
		this.c_nom = c_nom;
	}

	public String getC_adresse() {
		return c_adresse;
	}

	public void setC_adresse(String c_adresse) {
		this.c_adresse = c_adresse;
	}

	public String getC_photo() {
		return c_photo;
	}

	public void setC_photo(String c_photo) {
		this.c_photo = c_photo;
	}

	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public List<CommandeFournisseur> getC_commandesFournisseur() {
		return c_commandesFournisseur;
	}

	public void setC_commandesFournisseur(List<CommandeFournisseur> c_commandesFournisseur) {
		this.c_commandesFournisseur = c_commandesFournisseur;
	}
	
	
	
}
