package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_vente")
public class Vente implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idVente;
	
    private String c_code;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date c_date;
    
    @OneToMany(mappedBy="c_vente")
    private List<LigneDeVente> c_lignesDeVente;
    
	public Long getC_idVente() {
		return c_idVente;
	}

	public void setC_idVente(Long c_idVente) {
		this.c_idVente = c_idVente;
	}

	public String getC_code() {
		return c_code;
	}

	public void setC_code(String c_code) {
		this.c_code = c_code;
	}

	public Date getC_date() {
		return c_date;
	}

	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}

	public List<LigneDeVente> getC_lignesDeVente() {
		return c_lignesDeVente;
	}

	public void setC_lignesDeVente(List<LigneDeVente> c_lignesDeVente) {
		this.c_lignesDeVente = c_lignesDeVente;
	}
	
	
	
}
