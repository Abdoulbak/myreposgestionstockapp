package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_article")
public class Article implements Serializable {
	
	@Id
	@GeneratedValue
	private Long c_idArticle;
	
	private String c_codeArticle;
	private String c_libelle;
	private BigDecimal c_prixUnitaireHT;
	private BigDecimal c_tauxTva;
	private BigDecimal c_prixUnitaireTTC;
	private String c_photo;
	
	@ManyToOne
	@JoinColumn(name="c_idCategorie")
	private Categorie c_categorie;
	
	public Article() {
		
	}
	
	public Long getIdArticle() {
		return c_idArticle;
	}

	public void setIdArticle(Long c_idArticle) {
		this.c_idArticle = c_idArticle;
	}

	public Long getC_idArticle() {
		return c_idArticle;
	}

	public void setC_idArticle(Long c_idArticle) {
		this.c_idArticle = c_idArticle;
	}

	public String getC_codeArticle() {
		return c_codeArticle;
	}

	public void setC_codeArticle(String c_codeArticle) {
		this.c_codeArticle = c_codeArticle;
	}

	public String getC_libelle() {
		return c_libelle;
	}

	public void setC_libelle(String c_libelle) {
		this.c_libelle = c_libelle;
	}

	public BigDecimal getC_prixUnitaireHT() {
		return c_prixUnitaireHT;
	}

	public void setC_prixUnitaireHT(BigDecimal c_prixUnitaireHT) {
		this.c_prixUnitaireHT = c_prixUnitaireHT;
	}

	public BigDecimal getC_tauxTva() {
		return c_tauxTva;
	}

	public void setC_tauxTva(BigDecimal c_tauxTva) {
		this.c_tauxTva = c_tauxTva;
	}

	public BigDecimal getC_prixUnitaireTTC() {
		return c_prixUnitaireTTC;
	}

	public void setC_prixUnitaireTTC(BigDecimal c_prixUnitaireTTC) {
		this.c_prixUnitaireTTC = c_prixUnitaireTTC;
	}

	public Categorie getC_categorie() {
		return c_categorie;
	}

	public void setC_categorie(Categorie c_categorie) {
		this.c_categorie = c_categorie;
	}

	public String getC_photo() {
		return c_photo;
	}

	public void setC_photo(String c_photo) {
		this.c_photo = c_photo;
	}
	
	
	

}
