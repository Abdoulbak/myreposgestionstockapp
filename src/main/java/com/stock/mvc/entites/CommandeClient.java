package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_commandClient")
public class CommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idCommandClient;
    private String c_code;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date c_date;
    
    @ManyToOne
	@JoinColumn(name="c_idClient")
	private Client c_client;
    
    @OneToMany(mappedBy="c_commandeClient")
	private List<LigneCommandeClient> c_ligneCommandClient;
	
	public Long getC_idCommandClient() {
		return c_idCommandClient;
	}

	public void setC_idCommandClient(Long c_idCommandClient) {
		this.c_idCommandClient = c_idCommandClient;
	}

	public Date getC_date() {
		return c_date;
	}

	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}

	public Client getC_client() {
		return c_client;
	}

	public void setC_client(Client c_client) {
		this.c_client = c_client;
	}

	public List<LigneCommandeClient> getC_ligneCommandClient() {
		return c_ligneCommandClient;
	}

	public void setC_ligneCommandClient(List<LigneCommandeClient> c_ligneCommandClient) {
		this.c_ligneCommandClient = c_ligneCommandClient;
	}

	public String getC_code() {
		return c_code;
	}

	public void setC_code(String c_code) {
		this.c_code = c_code;
	}
	
	
	
	
}
