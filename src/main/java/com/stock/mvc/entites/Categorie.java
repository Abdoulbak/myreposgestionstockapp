package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_categorie")
public class Categorie implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idCategorie;
    private String c_code;
    private String c_libelle;
    
    @OneToMany(mappedBy="c_categorie")
    private List<Article> articles;
    
    public Categorie() {
	
	}
    
	public Long getIdCategorie() {
		return c_idCategorie;
	}

	public void setIdCategorie(Long c_idCategorie) {
		this.c_idCategorie = c_idCategorie;
	}

	public Long getC_idCategorie() {
		return c_idCategorie;
	}

	public void setC_idCategorie(Long c_idCategorie) {
		this.c_idCategorie = c_idCategorie;
	}

	public String getC_code() {
		return c_code;
	}

	public void setC_code(String c_code) {
		this.c_code = c_code;
	}

	public String getC_libelle() {
		return c_libelle;
	}

	public void setC_libelle(String c_libelle) {
		this.c_libelle = c_libelle;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
	
	
}
