package com.stock.mvc.entites;

import java.io.Serializable;
import java.rmi.activation.ActivationGroupDesc.CommandEnvironment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_ligneCommandClient")
public class LigneCommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idLigneCommandClient;

	@ManyToOne
	@JoinColumn(name="c_idArticle")
	private Article c_article;
	
	@ManyToOne
	@JoinColumn(name="c_idCommandClient")
	private CommandeClient c_commandeClient;
	
	public Long getC_idLigneCommandClient() {
		return c_idLigneCommandClient;
	}

	public void setC_idLigneCommandClient(Long c_idLigneCommandClient) {
		this.c_idLigneCommandClient = c_idLigneCommandClient;
	}

	public Article getC_article() {
		return c_article;
	}

	public void setC_article(Article c_article) {
		this.c_article = c_article;
	}

	public CommandeClient getC_commandeClient() {
		return c_commandeClient;
	}

	public void setC_commandeClient(CommandeClient c_commandeClient) {
		this.c_commandeClient = c_commandeClient;
	}
	
	
	
	
}
