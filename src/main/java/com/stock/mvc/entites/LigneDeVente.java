package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_ligneDeVente")
public class LigneDeVente implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idLigneDeVente;

	@ManyToOne
	@JoinColumn(name="c_idArticle")
	private Article c_article;
	
	@ManyToOne
	@JoinColumn(name="c_vente")
	private Vente c_vente ;
	
	public Long getC_idLigneDeVente() {
		return c_idLigneDeVente;
	}

	public void setC_idLigneDeVente(Long c_idLigneDeVente) {
		this.c_idLigneDeVente = c_idLigneDeVente;
	}

	public Article getC_article() {
		return c_article;
	}

	public void setC_article(Article c_article) {
		this.c_article = c_article;
	}

	public Vente getC_vente() {
		return c_vente;
	}

	public void setC_vente(Vente c_vente) {
		this.c_vente = c_vente;
	}

	
	
	
}
