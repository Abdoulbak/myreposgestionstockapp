package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_commandFournisseur")
public class CommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idCommandFournisseur;

	@Temporal(TemporalType.TIMESTAMP)
	private Date c_date;
    
    @ManyToOne
	@JoinColumn(name="c_idFournisseur")
	private Client c_fournisseur;
    
    @OneToMany(mappedBy="c_commandeFournisseur")
	private List<LigneCommandeFournisseur> c_ligneCommandFournisseur;
    
	public Long getC_idCommandFournisseur() {
		return c_idCommandFournisseur;
	}

	public void setC_idCommandFournisseur(Long c_idCommandFournisseur) {
		this.c_idCommandFournisseur = c_idCommandFournisseur;
	}

	public Date getC_date() {
		return c_date;
	}

	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}

	public Client getC_fournisseur() {
		return c_fournisseur;
	}

	public void setC_fournisseur(Client c_fournisseur) {
		this.c_fournisseur = c_fournisseur;
	}

	public List<LigneCommandeFournisseur> getC_ligneCommandFournisseur() {
		return c_ligneCommandFournisseur;
	}

	public void setC_ligneCommandFournisseur(List<LigneCommandeFournisseur> c_ligneCommandFournisseur) {
		this.c_ligneCommandFournisseur = c_ligneCommandFournisseur;
	}
	
	
	
}
