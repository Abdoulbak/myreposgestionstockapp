package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_ligneCommandFournisseur")
public class LigneCommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long c_idLigneCommandFournisseur;

	@Temporal(TemporalType.TIMESTAMP)
	private Date c_date;
    
	@ManyToOne
	@JoinColumn(name="c_idArticle")
	private Article c_article;
	
	@ManyToOne
	@JoinColumn(name="c_idCommandFournisseur")
	private CommandeClient c_commandeFournisseur;
    
	public Long getC_idLigneCommandFournisseur() {
		return c_idLigneCommandFournisseur;
	}

	public void setC_idLigneCommandFournisseur(Long c_idLigneCommandFournisseur) {
		this.c_idLigneCommandFournisseur = c_idLigneCommandFournisseur;
	}

	public Date getC_date() {
		return c_date;
	}

	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}

	public Article getC_article() {
		return c_article;
	}

	public void setC_article(Article c_article) {
		this.c_article = c_article;
	}

	public CommandeClient getC_commandeFournisseur() {
		return c_commandeFournisseur;
	}

	public void setC_commandeFournisseur(CommandeClient c_commandeFournisseur) {
		this.c_commandeFournisseur = c_commandeFournisseur;
	}


	
	
	
}
