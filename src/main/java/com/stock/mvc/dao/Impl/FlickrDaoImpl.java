package com.stock.mvc.dao.Impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.stock.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flickr;
	private UploadMetaData uploadMetaData = new UploadMetaData();
	private String apiKey ="971f0bfe55d593366a4a0e7ad57ba0a0";
	private String sharedSecret ="fad0abe1719d5ce0";
	
	private void connect() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157701103957442-94c327499a5eeffa");
		auth.setTokenSecret("4e4a85b26958f9d0");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
		
	}
	
	public void auth() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		
		AuthInterface authInterface = flickr.getAuthInterface();
		
		Token token = authInterface.getRequestToken();
		System.out.println("token: " + token);
		
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		
		System.out.println("Follow this URL to authorise yourself on Flick");
		System.out.println(url);
		System.out.println("Paste in the token it gives you:");
		System.out.println(">>");
		
		String tokenKey=JOptionPane.showInputDialog(null);
		
		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Authentification réussi");
		
		Auth auth = null;
		try {
			auth = authInterface.checkToken(requestToken);
		} catch(FlickrException e) {
			e.printStackTrace();
		}
		// les identifications
		System.out.println("Token: "+ requestToken.getToken());
		System.out.println("Secret: "+requestToken.getSecret());
		System.out.println("nsid: " +auth.getUser().getId());
		System.out.println("RealName: " +auth.getUser().getRealName());
		System.out.println("UserName: " +auth.getUser().getUsername());
		System.out.println("Permission: " +auth.getPermission().getType());
	}

}
