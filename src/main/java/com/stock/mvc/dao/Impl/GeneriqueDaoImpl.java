package com.stock.mvc.dao.Impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.mvc.dao.IGeneriqueDao;

public class GeneriqueDaoImpl<E> implements IGeneriqueDao<E> {

	@PersistenceContext
	EntityManager em;
	private Class<E> type;
	
	@SuppressWarnings("unchecked")
	public GeneriqueDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType)t;
		type =(Class<E>)pt.getActualTypeArguments()[0];
	}

	@Override
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		return em.merge(entity);
	}

	@Override
	public List<E> selectAll() {
	    Query query = em.createQuery("select t from " +type.getSimpleName()+"t");
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {
		return em.find(type,id);
	}
	
	@Override
	public void remove(Long id) {
	E tab=em.getReference(type,id);
	em.remove(tab);
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("select t from " +type.getSimpleName()+" t order by "+sortField+""+sort);
		return null;
	}

	@Override
	public E findOneBy(String paramName, Object paramValue) {
		Query query = em.createQuery("select t from " +type.getSimpleName()+" t where "+paramName+ " = :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public E findOneBy(String[] paramNames, Object[] paramValue) {
		if(paramNames.length != paramValue.length) {
			return null;
		}
		String queryString = "select e from "+ type.getSimpleName()+ " e where";
		int taille=paramNames.length;
		for(int i=0; i < taille; i++) {
			queryString +=" e." + paramNames[i]+ "= :x" + i;
			if((i+1) < taille) {
				queryString += " and ";
			}
		}
		Query query = em.createQuery(queryString);
		for(int i=0; i<0; i++) {
			query.setParameter("x"+i, paramValue[i]);
		}
		E rs = query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
		return rs;
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		Query query = em.createQuery("select t from " +type.getSimpleName()+" t where "+paramName+ " = :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0;
	}

	public Class<E> getType() {
		return type;
	}

	public void setType(Class<E> type) {
		this.type = type;
	}


	
	

}
