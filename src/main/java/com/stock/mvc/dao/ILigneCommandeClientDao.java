package com.stock.mvc.dao;

import com.stock.mvc.entites.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGeneriqueDao<LigneCommandeClient> {

}
