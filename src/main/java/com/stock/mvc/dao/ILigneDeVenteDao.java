package com.stock.mvc.dao;

import com.stock.mvc.entites.LigneDeVente;

public interface ILigneDeVenteDao extends IGeneriqueDao<LigneDeVente> {

}
