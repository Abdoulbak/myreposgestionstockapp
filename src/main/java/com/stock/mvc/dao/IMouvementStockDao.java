package com.stock.mvc.dao;

import com.stock.mvc.entites.MouvementStock;

public interface IMouvementStockDao extends IGeneriqueDao<MouvementStock> {

}
