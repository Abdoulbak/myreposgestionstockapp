package com.stock.mvc.dao;

import com.stock.mvc.entites.LigneCommandeFournisseur;

public interface ILigneCommandFournisseurDao extends IGeneriqueDao<LigneCommandeFournisseur> {

}
