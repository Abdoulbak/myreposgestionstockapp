package com.stock.mvc.dao;

import java.util.List;

public interface IGeneriqueDao<E> {

	/**
	 * Permet de sauvegarder un element en base
	 * @param entity
	 * @return
	 */
	public E save(E entity);
	/**
	 * Permet de mettre à jour un element en base
	 * @param entity
	 * @return
	 */
	public E update(E entity);
	/**
	 * Retourne la liste des elements en base
	 * @return
	 */
	public List<E> selectAll();
	/**
	 * Permet de chercher un element en base par son identifisant
	 * @param id
	 * @return
	 */
	public E getById(Long id);
	/**
	 * Permet de supprimer un element en base
	 * @param id
	 */
	public void remove(Long id);
	/**
	 * 
	 * @param sortField
	 * @param sort
	 * @return
	 */
	public List<E> selectAll(String sortField,String sort);
	/**
	 * Retourne la liste des element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public E findOneBy(String paramName,Object paramValue);
	/**
	 * Permet de rechercher un element en base
	 * @param paramNames
	 * @param paramValue
	 * @return
	 */
	public E findOneBy(String[] paramNames,Object[] paramValue);
	/**
	 * Permet de compter le nombre d'un element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public int findCountBy(String paramName,String paramValue);
}
