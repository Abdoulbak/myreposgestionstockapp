package com.stock.mvc.services;

import java.util.List;
import com.stock.mvc.entites.Fournisseur;

public interface IFournisseurService {

	/**
	 * Permet de sauvegarder un élément en base
	 * @param entity
	 * @return
	 */
	public Fournisseur save(Fournisseur entity);
	/**
	 * Permet de mettre à jour un élément en base
	 * @param entity
	 * @return
	 */
	public Fournisseur update(Fournisseur entity);
	/**
	 * Retourne la liste des elements en base
	 * @return
	 */
	public List<Fournisseur> selectAll();
	/**
	 * Permet de chercher un element en base par son identifisant
	 * @param id
	 * @return
	 */
	public Fournisseur getById(Long id);
	/**
	 * Permet de supprimer un element en base
	 * @param id
	 */
	public void remove(Long id);
	/**
	 * 
	 * @param sortField
	 * @param sort
	 * @return
	 */
	public List<Fournisseur> selectAll(String sortField,String sort);
	/**
	 * Retourne la liste des elements en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public Fournisseur findOneBy(String paramName,Object paramValue);
	/**
	 * Permet de rechercher un element en base
	 * @param paramNames
	 * @param paramValue
	 * @return
	 */
	public Fournisseur findOneBy(String[] paramNames,Object[] paramValue);
	/**
	 * Permet de compter le nombre d'un element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public int findCountBy(String paramName,String paramValue);
}
