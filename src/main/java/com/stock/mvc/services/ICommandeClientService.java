package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.CommandeClient;

public interface ICommandeClientService {


	/**
	 * Permet de sauvegarder un element en base
	 * @param entity
	 * @return
	 */
	public CommandeClient save(CommandeClient entity);
	/**
	 * Permet de mettre à jour un element en base
	 * @param entity
	 * @return
	 */
	public CommandeClient update(CommandeClient entity);
	/**
	 * Retourne la liste des elements en base
	 * @return
	 */
	public List<CommandeClient> selectAll();
	/**
	 * Permet de chercher un element en base par son identifisant
	 * @param id
	 * @return
	 */
	public CommandeClient getById(Long id);
	/**
	 * Permet de supprimer un element en base
	 * @param id
	 */
	public void remove(Long id);
	/**
	 * 
	 * @param sortField
	 * @param sort
	 * @return
	 */
	public List<CommandeClient> selectAll(String sortField,String sort);
	/**
	 * Retourne la liste des elements en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public CommandeClient findOneBy(String paramName,Object paramValue);
	/**
	 * Permet de rechercher un element en base
	 * @param paramNames
	 * @param paramValue
	 * @return
	 */
	public CommandeClient findOneBy(String[] paramNames,Object[] paramValue);
	/**
	 * Permet de compter le nombre d'un element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public int findCountBy(String paramName,String paramValue);
}
