package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.LigneCommandeClient;

public interface ILigneCommandeClientService {

	/**
	 * Permet de sauvegarder un element en base
	 * @param entity
	 * @return
	 */
	public LigneCommandeClient save(LigneCommandeClient entity);
	/**
	 * Permet de mettre à jour un element en base
	 * @param entity
	 * @return
	 */
	public LigneCommandeClient update(LigneCommandeClient entity);
	/**
	 * Retourne la liste des elements en base
	 * @return
	 */
	public List<LigneCommandeClient> selectAll();
	/**
	 * Permet de chercher un element en base par son identifisant
	 * @param id
	 * @return
	 */
	public LigneCommandeClient getById(Long id);
	/**
	 * Permet de supprimer un element en base
	 * @param id
	 */
	public void remove(Long id);
	/**
	 * 
	 * @param sortField
	 * @param sort
	 * @return
	 */
	public List<LigneCommandeClient> selectAll(String sortField,String sort);
	/**
	 * Retourne la liste des elements en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public LigneCommandeClient findOneBy(String paramName,Object paramValue);
	/**
	 * Permet de rechercher un element en base
	 * @param paramNames
	 * @param paramValue
	 * @return
	 */
	public LigneCommandeClient findOneBy(String[] paramNames,Object[] paramValue);
	/**
	 * Permet de compter le nombre d'un element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public int findCountBy(String paramName,String paramValue);
}
