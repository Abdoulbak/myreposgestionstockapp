package com.stock.mvc.services;

import java.util.List;
import com.stock.mvc.entites.CommandeFournisseur;

public interface ICommandeFournisseurService {

	/**
	 * Permet de sauvegarder un element en base
	 * @param entity
	 * @return
	 */
	public CommandeFournisseur save(CommandeFournisseur entity);
	/**
	 * Permet de mettre à jour un element en base
	 * @param entity
	 * @return
	 */
	public CommandeFournisseur update(CommandeFournisseur entity);
	/**
	 * Retourne la liste des elements en base
	 * @return
	 */
	public List<CommandeFournisseur> selectAll();
	/**
	 * Permet de chercher un element en base par son identifisant
	 * @param id
	 * @return
	 */
	public CommandeFournisseur getById(Long id);
	/**
	 * Permet de supprimer un element en base
	 * @param id
	 */
	public void remove(Long id);
	/**
	 * 
	 * @param sortField
	 * @param sort
	 * @return
	 */
	public List<CommandeFournisseur> selectAll(String sortField,String sort);
	/**
	 * Retourne la liste des elements en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public CommandeFournisseur findOneBy(String paramName,Object paramValue);
	/**
	 * Permet de rechercher un element en base
	 * @param paramNames
	 * @param paramValue
	 * @return
	 */
	public CommandeFournisseur findOneBy(String[] paramNames,Object[] paramValue);
	/**
	 * Permet de compter le nombre d'un element en base
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public int findCountBy(String paramName,String paramValue);
}
