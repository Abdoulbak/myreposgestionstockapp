package com.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneDeVenteDao;
import com.stock.mvc.entites.LigneDeVente;
import com.stock.mvc.services.ILigneDeVenteService;

@Transactional
public class LigneDeVenteServiceImpl implements ILigneDeVenteService {

	private ILigneDeVenteDao dao;
	
	@Override
	public LigneDeVente save(LigneDeVente entity) {
		return dao.save(entity);
	}

	@Override
	public LigneDeVente update(LigneDeVente entity) {
		return dao.update(entity);
	}

	@Override
	public List<LigneDeVente> selectAll() {
		return dao.selectAll();
	}

	@Override
	public LigneDeVente getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public List<LigneDeVente> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneDeVente findOneBy(String paramName, Object paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}

	@Override
	public LigneDeVente findOneBy(String[] paramNames, Object[] paramValue) {
		return dao.findOneBy(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	public void setDao(ILigneDeVenteDao dao) {
		this.dao = dao;
	}
	
	

}
