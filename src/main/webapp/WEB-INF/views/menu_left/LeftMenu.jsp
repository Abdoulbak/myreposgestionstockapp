<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Recherche...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<li><a href="index.html"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.dashboard"/></a></li>
			<li><a href="index.html"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.article"/></a></li>
			<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>
					<fmt:message key="common.client"/><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="flot.html"><fmt:message key="common.client"/></a></li>
					<li><a href="morris.html"><fmt:message key="common.client.commandes"/></a></li>
				</ul> <!-- /.nav-second-level --></li>
			<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>
					<fmt:message key="common.fournisseur"/><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="flot.html"><fmt:message key="common.fournisseur"/></a></li>
					<li><a href="morris.html"><fmt:message key="common.fournisseur.commandes"/></a></li>
				</ul> <!-- /.nav-second-level --></li>
			<li><a href="tables.html"><i class="fa fa-table fa-fw"></i>
					<fmt:message key="common.stock"/></a></li>
			<li><a href="forms.html"><i class="fa fa-edit fa-fw"></i>
					<fmt:message key="common.vente"/></a></li>
			<li><a href="#"><i class="fa fa-wrench fa-fw"></i> 
			<fmt:message key="common.parametrage"/><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="panels-wells.html"><fmt:message key="common.parametrage.utilisateurs"/></a></li>
					<li><a href="buttons.html"><fmt:message key="common.parametrage.categories"/></a></li>
				</ul> <!-- /.nav-second-level --></li>
<!-- 			<li><a href="#"><i class="fa fa-sitemap fa-fw"></i>
					Multi-Level Dropdown<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="#">Second Level Item</a></li>
					<li><a href="#">Second Level Item</a></li>
					<li><a href="#">Third Level <span class="fa arrow"></span></a>
						<ul class="nav nav-third-level">
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
						</ul> /.nav-third-level</li> -->
				</ul> <!-- /.nav-second-level --></li>
<!-- 			<li class="active"><a href="#"><i
					class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="active" href="blank.html">Blank Page</a></li>
					<li><a href="login.html">Login Page</a></li>
				</ul> /.nav-second-level</li> -->
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->